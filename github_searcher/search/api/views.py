from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from github_searcher.search.client import GitHubClient


class SearchView(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request):
        query = request.query_params.get("q")
        search_type = request.query_params.get("search_type")
        if not query:
            return Response(
                data={"message": "Search text is required"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        client = GitHubClient()
        response = client.search(query, search_type=search_type)
        return Response(data=response.json(), status=response.status_code)
