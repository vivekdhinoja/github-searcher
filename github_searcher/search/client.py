import requests
from django.conf import settings


class GitHubClient:
    def __init__(self):
        self.base_url = settings.GITHUB_BASE_URL
        self.ACCESS_TOKEN = settings.GITHUB_ACCESS_TOKEN

    def search(self, query, search_type=None):
        search_url = f"{self.base_url}search/{search_type}"
        headers = {"accept": "application/vnd.github+json"}
        if search_type == "issues":
            query = f"{query} type:issue"
        params = {"q": query}
        response = requests.get(search_url, params=params, headers=headers)
        return response
